<footer class="text-center text-lg-start text-white" 
        style="background-color: #929fba">

    <!-- Grid container -->
    <div class="container p-4 pb-0">
        <!-- Section: Links -->
        <section class="">
            <!--Grid row-->
            <div class="row">
                <hr class="w-100 clearfix d-md-none" />

                <div class="rounded-circle bg-white shadow-1-strong d-flex align-items-center justify-content-center mb-4 mx-auto" style="width: 150px; height: 150px;">
                    @php
                            $details = App\Models\CompanyDetail::all()
                            // display categories in navbar
                        @endphp
                    @foreach ($details as $detail)
                    <img src="{{ asset('http://localhost:8080/pet_adoption_admin/public/uploads/company/'). $detail->logo }}" class="img-fluid w-100" alt="logo-placeholder"/>
                    @endforeach
                </div>

                <hr class="w-100 clearfix d-md-none" />

                <!-- Grid column -->
                <hr class="w-100 clearfix d-md-none" />

                <!-- Grid column -->
                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                    @foreach ($details as $detail)
                    <h6 class="text-uppercase mb-4 font-weight-bold">Contact Us</h6>
                    <p><i class="fas fa-home mr-3"></i> {{$detail->address}}</p>
                    <p><i class="fas fa-phone mr-3"></i> {{$detail->contact_no}}</p>
                    <p><i class="fas fa-envelope mr-3"></i> {{$detail->email}}</p>
                    @endforeach
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Follow us</h6>

                    <!-- Facebook -->
                    <a
                    class="fb-color btn btn-primary btn-floating m-1 border-0"
                    href="#!"
                    role="button">
                        <i class="fab fa-facebook-f"></i>
                    </a>

                    <!-- Instagram -->
                    <a
                    class="insta-color btn btn-primary btn-floating m-1 border-0"
                    href="#!"
                    role="button">
                        <i class="fab fa-instagram"></i>
                    </a>

                    <!-- Whatsapp -->
                    <a
                    class="wa-color btn btn-primary btn-floating m-1 border-0"
                    href="#!"
                    role="button">
                        <i class="fab fa-whatsapp"></i>
                    </a>
                </div>
            </div>

        </section>
      <!-- End Section: Links -->
    </div>
    <!-- End Grid container -->

    <!-- Copyright -->
    <div
      class="text-center p-3"
      style="background-color: rgba(0, 0, 0, 0.2)">
      © 2022 Copyright:
      <a class="text-white">Company.com</a>
    </div>
    <!-- End Copyright -->
</footer>
<!-- End Footer -->
