<header class="section-header">
    <section class="header-main">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{route('homepage')}}">Logo</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#petAdoptNav"
                    aria-controls="petAdoptNav" aria-expanded="true" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="navbar-collapse collapse show" id="petAdoptNav">
                    <ul class="navbar-nav nav-pills nav-fill">
                        <li class="nav-item border-end">
                            <a class="nav-link  {{ Request::is('/') ? 'active pet-nav-active fw-bold' : '' }}"
                                href=" {{ url('/') }} ">
                                <i class="fa-solid fa-house"></i>
                                Home</a>
                        </li>
  
                        <li class="nav-item border-end">
                            <a class="nav-link {{ Request::is('pet-adopt-list') ||Request::is('pet-profile/*')  ? 'active pet-nav-active fw-bold' : '' }}"
                                href=" {{ route('pet-adopt-list') }} ">
                                <i class="fa-solid fa-paw"></i> Adopt List</a>
                        </li>
                    </ul>
                    <ul class=" navbar-nav ms-auto">
                        <li class="nav-item dropdown">
                            @auth
                            <li class="nav-item">
                                <a href="{{route('pet-adopt-request')}}" class="nav-link">
                                    <i class="fas fa-box-open"></i>My Pet Request Tracker</a>
                                </li>  
                                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i>
                                    Hello, {{auth()->user()->name}}</a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{route('logout')}}">
                                        Logout
                                    </a>
                                </ul>
                        </li>
                      
                    @endauth

                    @guest
                        <li class="nav-item me-3">
                            <a class="nav-link" type="button" href="{{ route('register') }}">Register</a>
                        </li>
                        <li class="nav-item me-3">
                            <a class="nav-link" type="button" href="{{ route('login') }}">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        @endguest
    </section>
</header>
