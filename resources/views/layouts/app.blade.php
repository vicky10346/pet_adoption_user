<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pet Adoption Website @yield('title')</title>

    {{-- css style --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href=" {{ asset('css/bootstrap.css') }} ">
    <link rel="stylesheet" href=" {{ asset('css/custom.css') }} ">

    {{-- the icons style --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    {{-- the font style --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;400;800;900&display=swap"
        rel="stylesheet">


</head>

<body>
    @include('inc.header')
    @yield('content')
    @include('inc.footer')


</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
{{-- Ajax jQuery autocomplete search --}}
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
<script>
    var availableTags = [];
      $.ajax({
        method:"GET",
        url:"/pet-adopt-list-search",
        success: function (response){
          // console.log(response);
          startAutoComplete(response);
        }
      })
    
    function startAutoComplete(availableTags){
      $("#pet_search").autocomplete({
        source: availableTags
    });
    }
  
</script>

</html>
