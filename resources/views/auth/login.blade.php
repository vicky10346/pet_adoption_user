@extends('layouts.app')
@section('title', 'Login')
@section('content')
    <main>
        <h1 class="text-center"> Login </h1>
        <div class="container w-75">
            <div class="justify-content-center m-4">
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show">
                    @foreach ($errors->all() as $error)
                        <div>{{$error}}</div>
                    @endforeach
                     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif  
                @if (session('status'))
                <div class="alert alert-danger alert-dismissible fade show">
                     {{ session('status') }}
                     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif  
                <form action="{{route('login')}}" method="POST">
                    @csrf
                    <div class="card shadow p-3 mb-5 bg-body rounded">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-12 mb-3">
                                    <label for="username"> Username </label>
                                    <input type="text" class="form-control
                                    @error('username')
                                     border-danger   
                                    @enderror"  id="username" name="username"
                                        placeholder="johndoe" value="{{old('username')}}">
                                </div>

                                <div class="col-md-12 mb-3">
                                    <label for="password"> Password</label>
                                    <input type="password" class="form-control @error('password')
                                    border-danger   
                                   @enderror" id="password" name="password"
                                        placeholder="Password" value="">
                                </div>


                                <div class="btn-group g-2 col-md-6 mx-auto">
                                    <button type="submit" class="btn btn-primary" role="button">Login</button>
                                    <a type="submit" class="btn btn-danger" role="button"
                                        href="{{ route('homepage') }}">Cancel</a>
                                </div>

                                <div class="g-2 col-md-12 d-flex justify-content-center">
                                    <span class="text-small">Click <a href="{{ url('register') }}"
                                            class="text-decoration-none">here</a> to register if you do not have an account</span>
                                </div>

                            </div>
                        </div>

                </form>

    </main>
@endsection
