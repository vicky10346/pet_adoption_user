@extends('layouts.app')
@section('title', 'Register')
@section('content')
    <main>
        <h1 class="text-center"> Register </h1>
        <div class="container w-75">
            <div class="justify-content-center m-4">
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show">
                    @foreach ($errors->all() as $error)
                        <div>{{$error}}</div>
                    @endforeach
                     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif  
                <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <div class="card shadow p-3 mb-5 bg-body rounded">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <label for="name"> Name </label>
                                    <input type="text" class="form-control @error('name')
                                     border-danger   
                                    @enderror" id="name" name="name" placeholder="John Doe" value="{{old('name')}}">

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="username"> Username </label>
                                    <input type="text" class="form-control @error('username')
                                    border-danger   
                                   @enderror" id="username" name="username"
                                        placeholder="johndoe" value="{{old('username')}}">
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="birthday"> Birthday </label>
                                    <input type="date" class="form-control @error('birthday')
                                    border-danger 
                                   @enderror" id="birthday" name="birthday" value="{{old('birthday')}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="address">Address</label>
                                    <textarea type="text" name="address" class="form-control @error('address') border-danger @enderror" cols="30"> {!!old('address')!!} </textarea>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="address">Email address</label>
                                    <input type="email" class="form-control @error('email') @enderror" id="email" name="email"
                                        placeholder="example@gmail.com" value="{{old('email')}}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="password"> Password</label>
                                    <input type="password" class="form-control @error('password')border-danger @enderror" id="password" name="password"
                                        placeholder="Password">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="password_confirmation">Confirm Password</label>
                                    <input type="password" class="form-control @error('password_confirmation') border-danger @enderror" id="confirmation_password"
                                        name="password_confirmation" placeholder="Password">
                                </div>

                                <div class="btn-group g-2 col-md-6 mx-auto">
                                    <button type="submit" class="btn btn-primary" role="button">Register</button>
                                    <a type="submit" class="btn btn-danger" role="button"
                                        href="{{ route('homepage') }}">Cancel</a>
                                </div>

                                <div class="g-2 col-md-12 d-flex justify-content-center">
                                  <span class="text-small">Click <a href="{{url('login')}}" class="text-decoration-none">here</a> to login</span>
                                </div>
                            </div>
                        </div>

                </form>

    </main>
@endsection
