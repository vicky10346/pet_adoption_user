@extends('layouts.app')
@section('title', 'Adopt List')
@section('content')
    <section id="pet-search">
        <div class="container">
            @if (session('message'))
                <div class="alert alert-dark alert-dismissible fade show" role="alert">
                    {{ session('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show">
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <div class="row g-2 align-items-center">
                <div class="col-md-12">
                    <h1 class="text-center"> Search a pet and adopt!</h1>
                </div>

                <form action="{{ url('searchpet') }}" method="POST" class="d-flex">
                    @csrf
                    <input class="form-control me-2" type="search" name="search_pet_name" id="pet_search"
                        placeholder="Search a pet name" aria-label="Search">
                    <button class="btn btn-outline-secondary" type="submit" title="Search">
                        <i class="fas fa-search"></i></button>
                </form>

            </div>
        </div>


    </section>

    <section id="pet-filter-bar">
        <div class="container">
            <div class="col-md-12">
                <div class="row mt-4">
                <div class="border shadow border-secondary rounded border-2">
                    <div class="p-1 my-1">
                        <h2 class="text-bold text-center">Filter Status</h2>
                    </div>
                    <div class="form-group">
                        <form action="#" method="GET">
                            <div class="p-1">
                                <label for="pet_age_category">Pet Age Category</label>
                                <select name="pet_age_category" id="pet_age_category" class="form-select"
                                    data-dependent="state">
                                </select>

                                <label for="pet_gender">Pet Gender</label>
                                <select name="pet_gender" id="pet_gender" class="form-select" data-dependent="state">

                                </select>

                            </div>
                            <div class="text-center">
                                <button class="btn btn-secondary m-1" type="submit" title="Filter your search">
                                    Filter
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <section id="pets-adopt-list">
        <div class="container">
            <div class="row">
                @foreach ($pets as $pet)
                    <div class="col-md-4 py-3">
                        <div class="col mb-4">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <h5 class="card-title text-center">{{ $pet->pet_name }}</h5>
                                    <p class="card-text text-center">{!! $pet->pet_description !!}</p>
                                    <div class="d-grid align-items-center">
                                        <div class="text-center">
                                            <a href="{{ url('/pet-profile/' . $pet->id) }}"
                                                class="btn btn-sm btn-primary">View</a>
                                        </div>

                                    </div>
                                </div>
                                <img id="pets-profile"
                                    src="{{ asset('http://localhost:8080/pet_adoption_admin/public/uploads/available-pets/') . $pet->pet_image }}"
                                    class="rounded img-fluid" alt="placeholder-3">
                            </div>
                        </div>

                    </div>
                @endforeach
                {{-- <div class="d-flex justify-content-center">{{ $pets->links('pagination::bootstrap-4') }}</div> --}}
            </div>
        </div>
        </div>
        </div>
    </section>


@endsection
