@extends('layouts.app')
@section('title', 'Home')

@section('content')

    <section id="carousel-pet">
        <div id="carousel-main" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="images/test-image2.jpg" class="img-fluid d-block" alt="cats-and-dogs">
                    <div class="carousel-caption">
                        @foreach ($details as $detail)
                        <h3>{{$detail->title}}</h3> 
                        @endforeach
                        <div class="d-flex justify-content-center">
                            <a href="#about-us" class="btn btn-primary">About Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="about-us" data-bs-spy="scroll" data-bs-target="#about-us" data-bs-offset="0" tabindex="0">
        <div class="container p-5">
            <h1 class="fw-light text-center">About Us</h1>
            @foreach ($details as $detail)
            <p class="text-muted">{!! $detail->description !!}</p>    
            @endforeach
            
        </div>

        <div class="container">
            <h1 class="text-center">Meet the pets!</h1>

            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                @foreach ($pets as $pet)
                    <div class="col mb-4">
                        <div class="card shadow-sm">
                            <div class="card-body">
                                <h5 class="card-title text-center">{{ $pet->pet_name }}</h5>
                                <p class="card-text text-center">{!! $pet->pet_description !!}</p>
                                <div class="d-grid align-items-center">
                                    <div class="text-center">
                                        <a href="{{ url('/pet-profile/' . $pet->id) }}" class="btn btn-sm btn-primary">View</a>
                                    </div>

                                </div>
                               
                            </div>
                            <img id="pets-retrieved" src="{{ asset('http://localhost:8080/pet_adoption_admin/public/uploads/available-pets/'). $pet->pet_image }}" class="rounded img-fluid"
                            alt="placeholder-3">

                        </div>

                    </div>
                @endforeach
            </div>

        </div>
    </section>

    <section id="announcements">
        <div class="container">

            <div id="carousel-announcements" class="carousel carousel-dark slide " data-bs-ride="carousel"
                data-bs-interval="false">

                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carousel-announcements" data-bs-slide-to="0"
                        class="active" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carousel-announcements" data-bs-slide-to="1"
                        aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carousel-announcements" data-bs-slide-to="2"
                        aria-label="Slide 3"></button>
                </div>

                <div class="carousel-inner">
                    @foreach ($announcements as $announcement => $slider)
                        <div class="carousel-item {{ $announcement == 1 ? 'active' : '' }}">
                            <div class="row g-1 m-4">
                                <div class="col-sm-5 col-md-4">
                                    <img src="{{ asset('http://localhost:8080/pet_adoption_admin/public/uploads/announcements/'). $slider->image }}" class="rounded img-fluid w-100"
                                        alt="placeholder-3">
                                </div>

                                <div class="col-sm-7 col-md-8 shadow bg-body rounded">

                                    <div class="p-3 text-center">
                                        <h3 class="card-title"> {{ $slider->title }}</h3>
                                        <small class="card-text">Created <span
                                                class="fst-italic">{{ $slider->created_at->format('d-m-Y h:i') }}</span></small>
                                        <p class="card-text text-center">
                                            {!! $slider->description !!}
                                        </p>

                                    </div>
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>

                <button class="carousel-control-prev" type="button" data-bs-target="#carousel-announcements"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carousel-announcements"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>

        </div>
    </section>




@endsection
