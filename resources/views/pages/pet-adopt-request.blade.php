@extends('layouts.app')
@section('title', 'Adoption Request')
@section('content')
    <h1 class="text-center">Adoption request</h1>
    <section id="requestcards">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @for ($i = 1; $i <= 3; $i++)
                        <div class="card w-100 mb-3">
                            <div class="row g-0">
                                <div class="card-header">Adoption Request #00{{ $i }}
                                    <span class="badge rounded-pill bg-secondary text-wrap">Pet ID: 0{{$i}} </span>
                                </div>
                                <div class="col-md-4">
                                    <img src="images/dog2.jpg" class="img-fluid d-block w-75 m-4" alt="placeholder-9"
                                        title="pet-to-adopt">
                                </div>
                                <div class="col-md-8">
                                    <ul class="list-group list-group-flush m-4">
                                        <li class="list-group-item">
                                            <h4>Pet Name </h4><span class="text-muted"> Pet Breed Type</span>
                                        </li>
                                        <li class="list-group-item"> <span class="fw-bold">Datetime of
                                                Request:</span> {{ date('d/m/Y') }} | {{ date('H:i') }} </li>
                                    </ul>
                                </div>
                            </div>
                            <hr>
                            <div class="container">
                                <p class="fs-4">Adoption Request Progress</p>
                                <div class="position-relative m-5">
                                    <div class="progress custom-progress-height">
                                        <div class="progress-bar" role="progressbar"
                                            aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <button type="button"
                                        class="position-absolute top-0 start-0 translate-middle btn btn-sm btn-primary rounded-pill">Requested</button><br>
                                    <button type="button"
                                        class="position-absolute top-0 start-50 translate-middle btn btn-sm btn-secondary rounded-pill">In
                                        Confirmation</button>
                                    <button type="button"
                                        class="position-absolute top-0 start-100 translate-middle btn btn-sm btn-secondary rounded-pill">Completed</button>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
    </section>

@endsection
