@extends('layouts.app')
@section('title', 'Profile')
@section('content')
    <section id="pet-profile">
        <div class="container">
            <div class="row">
                <div class="col-md-4 my-4">
                    <div class="card shadow-sm">
                        <img id="pets-profile"
                            src="{{ asset('http://localhost:8080/pet_adoption_admin/public/uploads/available-pets/') . $pet->pet_image }}"
                            class="rounded img-fluid" alt="placeholder-3">
                    </div>
                </div>
                <div class="col-md-8 my-4">
                    <div id="pet-detail" class="card w-100">
                        <div class="card-header">
                            <h3>{{ $pet->pet_name }}</h3>
                            <p class="text-small text-muted">{{ $pet->pet_breed }}</p>
                        </div>
                        <p class="card-text m-3">{{ $pet->pet_description }}</p>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{ $pet->pet_age }}</li>
                            <li class="list-group-item">{{ $pet->pet_gender }}</li>
                        </ul>
                        @auth
                            <div class="m-3 d-grid g-2 d-md-block">
                                <a class="btn btn-primary" href="{{ route('pet-adopt-request') }} " role="button"
                                    onclick="return confirm('Are you sure to adopt this pet?')">Adopt Now!</a>
                            </div>
                        @endauth
                        @guest
                            <div class="m-3 d-grid g-2 d-md-block">
                                <a class="btn btn-primary" href="{{ route('login') }} " role="button"
                                    onclick="return confirm('Please register/log in to continue.')">Adopt Now!</a>
                            </div>
                        @endguest

                    </div>
                </div>
            </div>
        </div>

    </section>


    <section id="comments-section">

        <div class="container">
            @if (session('message'))
                <div class="alert alert-dark alert-dismissible fade show" role="alert">
                    {{ session('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif


            @auth
                <form action="{{ url('comments') }}" method="POST" class="d-flex mb-3">
                    @csrf
                    <input type="hidden" name="pet_id" value="{{ $pet->id }}" />
                    <input class="form-control" type="text" placeholder="Leave your comments here" name="comment" id="comment"
                        required>
                    <button class="btn btn-secondary" type="submit" title="submit your comment">
                        <i class="fas fa-paper-plane"></i></button>
                </form>
            @endauth

            <div class="row">
                @forelse ($pet->comments as $comment)
                    <div class="col-md-12">
                        <div class="users-section p-4 mb-5">
                            <p class="fs-4 d-inline me-4 fst-italic">
                                @if ($comment->user)
                                    {{ $comment->user->username }}
                                @endif
                            </p>
                            <span class="fw-bold">Commented on: {{ $comment->created_at->format('d/m/Y') }}
                            </span>
                            <hr>
                            <p class="mb-1">
                                {!! $comment->comment !!}
                            </p>
                        </div>
                    </div>
                @empty
                    <div class="col-md-12">
                        <div class="users-section p-4 mb-5">
                            <p class="fs-4 d-inline me-4 fst-italic">
                                No comments made here. 😞
                            </p>
                        </div>
                    </div>
                @endforelse
            </div>
        </div>

    </section>
@endsection
