<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PetController;
use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','HomepageController@index')->name('homepage');

// Route for signing up/register
Route::get('/register', 'Auth\RegisterController@index')->name('register');
Route::post('/register','Auth\RegisterController@store');

// Route for logging in
Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post('/login','Auth\LoginController@store');

Route::get('/logout','Auth\LogoutController@store')->name('logout');


// Pets available in the adoption center/sanctuary
Route::get('/pet-adopt-list', 'PetController@index')
->name('pet-adopt-list');
Route::get('pet-adopt-list-search','PetController@petlistAjax');
Route::post('searchpet','PetController@searchPetName');



// Route for viewing a pet's profile
Route::get('/pet-profile/{id}','PetProfileController@index')->name('pet-profile');

// Route to redirect users to pet adoption request profile pic
Route::get('pet-adopt-request', function(){
    return view ('pages.pet-adopt-request');
})->name('pet-adopt-request');

// Route for providing comments
Route::post('comments','CommentController@store');

//Route for search

