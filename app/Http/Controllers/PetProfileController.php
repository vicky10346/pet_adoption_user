<?php

namespace App\Http\Controllers;

use App\Models\Pet;

use Illuminate\Http\Request;

class PetProfileController extends Controller
{
    public function index($id)
    {
        $pet = Pet::find($id);
        return view('pages.pet-profile', compact('pet'));
    }
}
