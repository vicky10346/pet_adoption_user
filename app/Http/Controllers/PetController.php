<?php

namespace App\Http\Controllers;

use App\Models\Pet;
use Illuminate\Http\Request;
use DB;

class PetController extends Controller
{
    public function index(){
        
        $pets=Pet::where('pet_availability','1')->get();
        return view ('pages.pet-adopt-list',compact('pets'));
    }

    public function petlistAjax(){
        $pets = Pet::select('pet_name')->where('pet_availability','1')->get();
        $data = [];
        
        foreach($pets as $pet){
            $data[] = $pet['pet_name'];
        }

        return $data;
        
    }

    public function searchPetName(Request $request)
    {
       $request->validate([
            'search_pet_name' => 'required|string'
        ]);

        $searched_pet = $request->search_pet_name;

        if($searched_pet != "")
        {
            $pet = Pet::where("pet_name","LIKE","%$searched_pet%")->first();
            if($pet)
            {
                return redirect('pet-profile/'.$pet->id);
            }else{
                return redirect()->back()->with('message','Sorry, pet name is not available');
            }
        }else{
            return redirect()->back();
        }
    }
}


