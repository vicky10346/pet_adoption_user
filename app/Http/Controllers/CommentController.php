<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Pet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store(Request $request){

        if(Auth::check()){
            $pet = Pet::where('id',$request->pet_id)->where('pet_availability','1')->first();

            if($pet){
                Comment::create([
                    'pet_id'=>$pet->id,
                    'user_id'=>Auth::user()->id,
                    'comment'=>$request->comment
                ]);
                return redirect()->back()->with('message','User has been commented successfully!');
            }
            else{
                return redirect()->back()->with('message','Something is going on, please try later');
            }
        }
    }

}
