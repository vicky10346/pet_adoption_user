<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use App\Models\CompanyDetail;
use App\Models\Pet;
use Illuminate\Http\Request;

class HomepageController extends Controller
{

   

    public function index()
    {
        $announcements=Announcement::where('status','1')->orderBy('created_at','DESC')->get()->take(3);
        $details=CompanyDetail::all();
        $pets=Pet::where('pet_availability','1')->inRandomOrder()->limit(3)->get();
        return view ('pages.homepage',compact('announcements','details','pets'));
    }
}
