<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    public function __construct()
    {
        $this->middleware(['guest']);
    }

    public function index(){
       return view('auth.register');  
    }

    public function store(Request $request){
        // dd($request->only('email','password')); 
        // validation
        $this->validate($request,[
            'name' => 'required|max:255',
            'username'=>'required|max:255',
            'email'=>'required|email|unique:users',
            'birthday'=>'required|date',
            'address'=>'required',
            'password'=>'required|confirmed|min:5'
        ]);
        // store the user
        // dd('store');
        // sign the user in
        User::create([
            'name'=> $request->name,
            'username'=> $request->username,
            'birthday'=> $request->birthday,
            'address'=>$request->address,
            'email'=>$request->email,
            'password'=>Hash::make($request->password)
            ]);
        // redirect 
        auth()->attempt($request->only('username','password'));

        return redirect()->route('homepage');
     }
   
}
