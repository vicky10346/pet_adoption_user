<?php

namespace App\Models;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    use HasFactory;

    protected $fillable = [
        'pet_name', 'pet_description'
    ];

    public function comments(){
        return $this->hasMany(Comment::class,'pet_id','id');
    }

 

}
