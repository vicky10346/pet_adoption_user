<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $table = 'comments';

    protected $fillable = [
        'user_id',
        'pet_id',
        'comment'
    ];

    public function pet(){
        return $this->belongsTo(Pet::class,'pet_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
